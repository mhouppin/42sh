/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vector.h                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/21 11:16:02 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:10:22 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_VECTOR_H
# define FT_VECTOR_H

# include "ft_string.h"

# define VECTOR_MUL(vec, index)	((vec)->data + (index) * (vec)->itemsize)
# define VECTOR_SHL(vec, index)	((vec)->data + ((index) << (vec)->shift))
# define UNSHIFTABLE			(sizeof(size_t) * 8)

struct		s_vector
{
	char	*data;
	size_t	itemsize;
	size_t	nitems;
	size_t	maxitems;
	int		(*compar)(const void *, const void *);
	int		(*filter)(const void *);
	void	(*delete)(void *);
	size_t	shift;
};

typedef struct s_vector	t_vector;

void		*ft_vat(const t_vector *vec, size_t index);
t_vector	*ft_vclone(const t_vector *other);
void		ft_vdelete(t_vector *vec, size_t index);
void		ft_vfilter(t_vector *vec);
size_t		ft_vindex(t_vector *vec, const void *elem);
t_vector	*ft_vinit(size_t itemsize);
int			ft_vinsert(t_vector *vec, const void *item, size_t index);
int			ft_vpush(t_vector *vec, const void *item);
void		ft_vquit(t_vector *vec);
void		ft_vreset(t_vector *vec);
void		*ft_vsearch(t_vector *vec, const void *elem);
void		ft_vsort(t_vector *vec);

int			ft_vrealloc(t_vector *vec);

#endif
