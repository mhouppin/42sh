/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_stdio.h                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:52:38 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/08 09:03:21 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_STDIO_H
# define FT_STDIO_H

# include <unistd.h>
# include <stdarg.h>

int		ft_dprintf(int fd, const char *fmt, ...);
int		ft_dputchar(int fd, char c);
int		ft_dputendl(int fd, const char *s);
int		ft_dputstr(int fd, const char *s);
ssize_t	ft_getline(int fd, char **line);
int		ft_printf(const char *fmt, ...);
int		ft_putchar(char c);
int		ft_putendl(const char *s);
int		ft_putstr(const char *s);
int		ft_vdprintf(int fd, const char *fmt, va_list ap);
int		ft_vprintf(const char *fmt, va_list ap);

#endif
