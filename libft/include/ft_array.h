/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_array.h                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 10:21:42 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 12:35:00 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_ARRAY_H
# define FT_ARRAY_H

# include "ft_string.h"
# include <stdint.h>

struct	s_array
{
	size_t	used_size;
	size_t	max_size;
	uint8_t	*data;
};

struct	s_block
{
	size_t	index;
	size_t	size;
};

typedef struct s_array	t_array;
typedef struct s_block	t_block;

t_array	*ft_binit(size_t size);
void	ft_bquit(t_array *b);

int		ft_binsert(t_array *b, size_t index, const void *data, size_t size);
int		ft_bpush_back(t_array *b, const void *data, size_t size);
int		ft_bpush_front(t_array *b, const void *data, size_t size);

int		ft_binsert_c(t_array *b, size_t index, int c);
int		ft_bpush_back_c(t_array *b, int c);
int		ft_bpush_front_c(t_array *b, int c);

int		ft_bset(t_array *b, size_t index, int c, size_t size);
int		ft_bset_back(t_array *b, int c, size_t size);
int		ft_bset_front(t_array *b, int c, size_t size);

int		ft_brealloc(t_array *b, size_t new_size);
int		ft_bauto_realloc(t_array *b);

void	ft_bdelete(t_array *b, size_t index, size_t size);
void	ft_bdelete_c(t_array *b, size_t index);

#endif
