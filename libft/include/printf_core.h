/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   printf_core.h                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 10:38:21 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/12 09:50:13 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef PRINTF_CORE_H
# define PRINTF_CORE_H

# include "ft_array.h"
# include <stdarg.h>
# include <stdint.h>

enum		e_flag
{
	FlagMinus = 1,
	FlagZero = 2,
	FlagPlus = 4,
	FlagSpace = 8,
	FlagSharp = 16,
	FieldPrecision = 32,
	Length_l = 64,
	Length_ll = 128,
	Length_h = 256,
	Length_hh = 512,
	Length_z = 1024,
	Length_j = 2048,
	Length_t = 4096,
	Length_all = Length_l | Length_ll | Length_h | Length_hh
	| Length_z | Length_j | Length_t
};

struct		s_format
{
	int		flags;
	int		width;
	int		precision;
	int		specifier;
};

typedef struct s_format	t_format;

const char	*print_conv(t_array *buffer, const char *fmt, va_list ap);
const char	*apply_width(t_array *buffer, t_array *conv, t_format *info,
			const char *fmt);

int			print_bin(t_array *to_fill, t_format *format, va_list ap);
int			print_chr(t_array *to_fill, t_format *format, va_list ap);
int			print_hex(t_array *to_fill, t_format *format, va_list ap);
int			print_int(t_array *to_fill, t_format *format, va_list ap);
int			print_oct(t_array *to_fill, t_format *format, va_list ap);
int			print_str(t_array *to_fill, t_format *format, va_list ap);
int			print_uns(t_array *to_fill, t_format *format, va_list ap);

uintmax_t	va_load_u(t_format *format, va_list ap);
intmax_t	va_load_i(t_format *format, va_list ap);

void		clean_format(t_format *format);
void		get_uint(t_array *to_fill, uintmax_t value, uintmax_t base);
void		ultoa_buf(char **ptr, uintmax_t value, uintmax_t base);
int			int_set_precision(t_array *to_fill, t_format *format);
int			int_sharp(t_array *to_fill, t_format *format);
int			int_zero_padding(t_array *to_fill, t_format *format, int sign);

#endif
