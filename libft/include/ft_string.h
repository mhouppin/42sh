/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_string.h                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 15:00:01 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 15:13:20 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_STRING_H
# define FT_STRING_H

# include <stddef.h>

void	ft_bzero(void *s, size_t n);
void	*ft_memccpy(void *dst, const void *src, int c, size_t n);
void	*ft_memchr(const void *s, int c, size_t n);
int		ft_memcmp(const void *s1, const void *s2, size_t n);
void	*ft_memcpy(void *dst, const void *src, size_t n);
void	*ft_memmem(const void *b, size_t blen, const void *l, size_t llen);
void	*ft_memmove(void *dst, const void *src, size_t len);
void	*ft_mempcpy(void *dst, const void *src, size_t n);
void	*ft_memrcpy(void *dst, const void *src, size_t n);
void	*ft_memrload(const void *b, size_t size, size_t access_times);
void	*ft_memset(void *b, int c, size_t len);
void	*ft_memwload(const void *b, size_t size, size_t access_times);
char	*ft_stpcpy(char *dst, const char *src);
char	*ft_stpncpy(char *dst, const char *src, size_t len);
char	*ft_strcat(char *s1, const char *s2);
char	*ft_strchr(const char *s, int c);
char	*ft_strchrnul(const char *s, int c);
void	ft_strclr(char *str);
int		ft_strcmp(const char *s1, const char *s2);
char	*ft_strcpy(char *dst, const char *src);
size_t	ft_strcspn(const char *str, const char *reject);
char	*ft_strdup(const char *s1);
int		ft_strequ(const char *s1, const char *s2);
char	*ft_strjoin(const char *s1, const char *s2);
size_t	ft_strlcat(char *dst, const char *src, size_t size);
size_t	ft_strlcpy(char *dst, const char *src, size_t size);
size_t	ft_strlen(const char *s);
char	*ft_strncat(char *s1, const char *s2, size_t n);
int		ft_strncmp(const char *s1, const char *s2, size_t n);
char	*ft_strncpy(char *dst, const char *src, size_t len);
char	*ft_strndup(const char *s1, size_t n);
int		ft_strnequ(const char *s1, const char *s2, size_t n);
size_t	ft_strnlen(const char *s, size_t maxlen);
char	*ft_strnstr(const char *big, const char *little, size_t n);
char	*ft_strrchr(const char *s, int c);
size_t	ft_strspn(const char *str, const char *accept);
char	*ft_strstr(const char *big, const char *little);
char	*ft_strtok(char *str, const char *delim);
char	*ft_strtok_r(char *str, const char *delim, char **last);
char	*ft_substr(const char *str, size_t start, size_t maxlen);
void	ft_swap(void *left, void *right, size_t size);

#endif
