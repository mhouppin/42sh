/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_stdlib.h                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:39:55 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 09:29:04 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_STDLIB_H
# define FT_STDLIB_H

# include <stdlib.h>

int		ft_atoi(const char *str);
void	*ft_calloc(size_t size, size_t nitems);
char	*ft_itoa(int val);
void	*ft_memalloc(size_t size);
void	ft_memdel(void **ptr);
void	ft_memkill(void **ptr, size_t size);
char	*ft_strnew(size_t nbytes);
void	ft_strdel(char **ptr);

#endif
