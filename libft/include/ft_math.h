/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_math.h                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 10:16:50 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/08 12:15:16 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_MATH_H
# define FT_MATH_H

# include <stddef.h>
# include <stdint.h>

size_t		ft_zsqrt(size_t x);
uintmax_t	ft_jsqrt(uintmax_t x);

#endif
