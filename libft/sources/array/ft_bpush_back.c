/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bpush_back.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 12:07:15 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:15:38 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_bpush_back(t_array *b, const void *data, size_t size)
{
	if (b->used_size + size > b->max_size)
		if (!ft_brealloc(b, b->used_size + size + size / 2))
			return (0);
	ft_memcpy(b->data + b->used_size, data, size);
	b->used_size += size;
	return (1);
}
