/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   brealloc.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 12:14:36 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:18:29 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"
#include <stdlib.h>

int		ft_brealloc(t_array *b, size_t new_size)
{
	uint8_t	*tmp;

	tmp = (uint8_t *)malloc(new_size);
	if (tmp == NULL)
		return (0);
	if (new_size < b->used_size)
		b->used_size = new_size;
	ft_memcpy(tmp, b->data, b->used_size);
	b->max_size = new_size;
	free(b->data);
	b->data = tmp;
	return (1);
}
