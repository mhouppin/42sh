/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_bdelete_c.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/10 15:32:33 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:33:17 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

void	ft_bdelete_c(t_array *b, size_t index)
{
	b->used_size -= 1;
	ft_memcpy(b->data + index, b->data + index + 1, b->used_size - index);
}
