/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   binsert.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 12:02:17 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 12:15:02 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_binsert(t_array *b, size_t index, const void *data, size_t size)
{
	if (b->used_size + size > b->max_size)
		if (!ft_brealloc(b, b->used_size + size + size / 2))
			return (0);
	ft_memrcpy(b->data + index + size, b->data + index, b->used_size - index);
	ft_memcpy(b->data + index, data, size);
	b->used_size += size;
	return (1);
}
