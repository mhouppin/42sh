/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bauto_realloc.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 12:55:25 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:13:17 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"
#include "ft_math.h"
#include <stdlib.h>

int		ft_bauto_realloc(t_array *b)
{
	size_t	new_size;
	uint8_t	*tmp;

	new_size = b->max_size + ft_zsqrt(b->max_size) + 1;
	tmp = (uint8_t *)malloc(new_size);
	if (tmp == NULL)
		return (0);
	ft_memcpy(tmp, b->data, b->used_size);
	free(b->data);
	b->data = tmp;
	b->max_size = new_size;
	return (1);
}
