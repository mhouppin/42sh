/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   binsert_c.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 13:02:36 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:15:03 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_binsert_c(t_array *b, size_t index, int c)
{
	if (b->used_size == b->max_size)
		if (!ft_bauto_realloc(b))
			return (0);
	ft_memrcpy(b->data + index + 1, b->data + index,
			b->used_size - index);
	b->data[index] = c;
	b->used_size += 1;
	return (1);
}
