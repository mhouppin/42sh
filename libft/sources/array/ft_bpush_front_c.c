/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bpush_front_c.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 13:06:50 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:17:10 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_bpush_front_c(t_array *b, int c)
{
	if (b->used_size == b->max_size)
		if (!ft_bauto_realloc(b))
			return (0);
	ft_memrcpy(b->data + 1, b->data, b->used_size);
	b->data[0] = c;
	b->used_size += 1;
	return (1);
}
