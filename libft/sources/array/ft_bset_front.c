/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_bset_front.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 12:17:15 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 12:18:39 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_bset_front(t_array *b, int c, size_t size)
{
	if (b->used_size + size > b->max_size)
		if (!ft_brealloc(b, b->used_size + size + size / 2))
			return (0);
	ft_memrcpy(b->data + size, b->data, b->used_size);
	ft_memset(b->data, c, size);
	b->used_size += size;
	return (1);
}
