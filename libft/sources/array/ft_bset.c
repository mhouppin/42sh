/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_bset.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 12:12:57 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 12:14:58 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_bset(t_array *b, size_t index, int c, size_t size)
{
	if (b->used_size + size > b->max_size)
		if (!ft_brealloc(b, b->used_size + size + size / 2))
			return (0);
	ft_memrcpy(b->data + index + size, b->data + index, b->used_size - index);
	ft_memset(b->data + index, c, size);
	b->used_size += size;
	return (1);
}
