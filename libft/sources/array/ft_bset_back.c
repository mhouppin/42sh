/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_bset_back.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 12:15:14 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 12:16:21 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_bset_back(t_array *b, int c, size_t size)
{
	if (b->used_size + size > b->max_size)
		if (!ft_brealloc(b, b->used_size + size + size / 2))
			return (0);
	ft_memset(b->data + b->used_size, c, size);
	b->used_size += size;
	return (1);
}
