/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_bdelete.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/10 15:30:52 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:32:09 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

void	ft_bdelete(t_array *b, size_t index, size_t size)
{
	b->used_size -= size;
	ft_memcpy(b->data + index, b->data + index + size, b->used_size - index);
}
