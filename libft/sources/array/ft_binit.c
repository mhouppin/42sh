/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   binit.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 12:00:09 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:13:42 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"
#include <stdlib.h>

t_array	*ft_binit(size_t size)
{
	t_array		*ret;

	ret = (t_array *)malloc(sizeof(t_array));
	if (ret == NULL)
		return (NULL);
	ret->data = (uint8_t *)malloc(size);
	if (ret->data == NULL)
	{
		free(ret);
		return (NULL);
	}
	ret->used_size = 0;
	ret->max_size = size;
	ft_bzero(ret->data, size);
	return (ret);
}
