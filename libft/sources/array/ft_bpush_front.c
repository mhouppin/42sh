/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bpush_front.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 12:09:13 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:16:39 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_bpush_front(t_array *b, const void *data, size_t size)
{
	if (b->used_size + size > b->max_size)
		if (!ft_brealloc(b, b->used_size + size + size / 2))
			return (0);
	ft_memrcpy(b->data + size, b->data, b->used_size);
	ft_memcpy(b->data, data, size);
	b->used_size += size;
	return (1);
}
