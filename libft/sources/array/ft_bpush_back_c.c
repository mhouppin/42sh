/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bpush_back_c.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/04 13:05:07 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 15:16:06 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_array.h"

int		ft_bpush_back_c(t_array *b, int c)
{
	if (b->used_size == b->max_size)
		if (!ft_bauto_realloc(b))
			return (0);
	b->data[b->used_size] = c;
	b->used_size += 1;
	return (1);
}
