/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_jsqrt.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 12:15:34 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/08 12:15:40 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_math.h"

uintmax_t	ft_jsqrt(uintmax_t x)
{
	uintmax_t		ret;
	uintmax_t		square;
	uintmax_t		threshold;

	ret = 0;
	threshold = (1ul << (4 * sizeof(uintmax_t) - 1));
	while (threshold)
	{
		square = (ret + threshold) * (ret + threshold);
		if (square <= x)
			ret += threshold;
		threshold >>= 1;
	}
	return (ret);
}
