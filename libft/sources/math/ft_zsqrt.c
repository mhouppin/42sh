/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_zsqrt.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 10:17:38 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/08 12:16:08 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_math.h"

size_t	ft_zsqrt(size_t x)
{
	size_t		ret;
	size_t		square;
	size_t		threshold;

	ret = 0;
	threshold = (1ul << (4 * sizeof(size_t) - 1));
	while (threshold)
	{
		square = (ret + threshold) * (ret + threshold);
		if (square <= x)
			ret += threshold;
		threshold >>= 1;
	}
	return (ret);
}
