/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vat.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/21 11:16:13 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/21 12:43:26 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

void	*ft_vat(const t_vector *vec, size_t index)
{
	if (index >= vec->nitems)
		return (NULL);
	if (vec->shift == UNSHIFTABLE)
		return (VECTOR_MUL(vec, index));
	else
		return (VECTOR_SHL(vec, index));
}
