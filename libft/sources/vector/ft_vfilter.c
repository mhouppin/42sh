/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vfilter.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/21 12:44:27 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/21 13:58:37 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

void	ft_vfilter(t_vector *vec)
{
	size_t	i;
	char	*elem;

	i = 0;
	elem = vec->data;
	while (i < vec->nitems)
	{
		if (vec->filter(elem))
			ft_vdelete(vec, i);
		else
		{
			elem += vec->itemsize;
			i++;
		}
	}
}
