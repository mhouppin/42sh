/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vinit.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 14:20:00 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 14:31:21 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"
#include <stdlib.h>

static size_t	tzcnt(size_t value)
{
	size_t	i;

	if (!value)
		return (UNSHIFTABLE);
	i = 0;
	while ((value & 1) == 0)
	{
		i++;
		value = (value >> 1);
	}
	return (i);
}

t_vector		*ft_vinit(size_t itemsize)
{
	t_vector	*vec;

	vec = (t_vector *)malloc(sizeof(t_vector));
	if (vec == NULL)
		return (NULL);
	vec->data = NULL;
	vec->itemsize = itemsize;
	vec->nitems = 0;
	vec->maxitems = 0;
	vec->compar = NULL;
	vec->filter = NULL;
	vec->delete = NULL;
	if (itemsize & (itemsize - 1))
		vec->shift = UNSHIFTABLE;
	else
		vec->shift = tzcnt(itemsize);
	return (vec);
}
