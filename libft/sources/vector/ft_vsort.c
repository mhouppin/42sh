/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vsort.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 16:13:11 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:26:14 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

void	ft_vsort_slow(t_vector *vec)
{
	size_t	gap;
	size_t	group;
	size_t	i;

	gap = vec->nitems / 2;
	while (gap > 0)
	{
		group = gap;
		while (group < vec->nitems)
		{
			i = group - gap;
			while (i + gap >= gap)
			{
				if (vec->compar(VECTOR_MUL(vec, i + gap),
					VECTOR_MUL(vec, i)) >= 0)
					break ;
				else
					ft_swap(VECTOR_MUL(vec, i + gap),
							VECTOR_MUL(vec, i), vec->itemsize);
				i -= gap;
			}
			group++;
		}
		gap /= 2;
	}
}

void	ft_vsort_fast(t_vector *vec)
{
	size_t	gap;
	size_t	group;
	size_t	i;

	gap = vec->nitems / 2;
	while (gap > 0)
	{
		group = gap;
		while (group < vec->nitems)
		{
			i = group - gap;
			while (i + gap >= gap)
			{
				if (vec->compar(VECTOR_SHL(vec, i + gap),
					VECTOR_SHL(vec, i)) >= 0)
					break ;
				else
					ft_swap(VECTOR_SHL(vec, i + gap),
						VECTOR_SHL(vec, i), vec->itemsize);
				i -= gap;
			}
			group++;
		}
		gap /= 2;
	}
}

void	ft_vsort(t_vector *vec)
{
	if (vec->shift == UNSHIFTABLE)
		ft_vsort_slow(vec);
	else
		ft_vsort_fast(vec);
}
