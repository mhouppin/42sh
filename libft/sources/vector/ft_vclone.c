/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vclone.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/21 11:53:54 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/21 12:43:41 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"
#include <stdlib.h>

t_vector	*ft_vclone(const t_vector *other)
{
	t_vector	*vec;

	vec = (t_vector *)malloc(sizeof(t_vector));
	if (vec == NULL)
		return (NULL);
	vec->data = (char *)malloc(other->itemsize * other->maxitems);
	if (vec->data == NULL)
	{
		free(vec);
		return (NULL);
	}
	ft_memcpy(vec->data, other->data, other->itemsize * other->nitems);
	vec->itemsize = other->itemsize;
	vec->nitems = other->nitems;
	vec->maxitems = other->maxitems;
	vec->compar = other->compar;
	vec->filter = other->filter;
	vec->delete = other->delete;
	vec->shift = other->shift;
	return (vec);
}
