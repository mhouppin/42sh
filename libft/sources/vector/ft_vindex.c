/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vindex.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/21 13:56:59 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 14:58:18 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

static size_t	ft_vindex_slow(t_vector *vec, const void *elem)
{
	size_t	left;
	size_t	right;
	size_t	i;

	left = 0;
	right = vec->nitems;
	while (left < right)
	{
		i = (left + right) / 2;
		if (vec->compar(elem, VECTOR_MUL(vec, i)) < 0)
			right = i;
		else
			left = i + 1;
	}
	return (left);
}

static size_t	ft_vindex_fast(t_vector *vec, const void *elem)
{
	size_t	left;
	size_t	right;
	size_t	i;

	left = 0;
	right = vec->nitems;
	while (left < right)
	{
		i = (left + right) / 2;
		if (vec->compar(elem, VECTOR_SHL(vec, i)) < 0)
			right = i;
		else
			left = i + 1;
	}
	return (left);
}

size_t			ft_vindex(t_vector *vec, const void *elem)
{
	if (vec->shift == UNSHIFTABLE)
		return (ft_vindex_slow(vec, elem));
	else
		return (ft_vindex_fast(vec, elem));
}
