/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vinsert.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 14:49:11 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 14:56:24 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

int		ft_vinsert(t_vector *vec, const void *item, size_t index)
{
	char	*pos;

	if (vec->nitems == vec->maxitems)
		if (!ft_vrealloc(vec))
			return (0);
	if (vec->shift == UNSHIFTABLE)
	{
		pos = VECTOR_MUL(vec, index);
		ft_memrcpy(pos + vec->itemsize, pos, (vec->nitems - index)
				* vec->itemsize);
		ft_memcpy(pos, item, vec->itemsize);
	}
	else
	{
		pos = VECTOR_SHL(vec, index);
		ft_memrcpy(pos + vec->itemsize, pos, (vec->nitems - index)
				<< vec->shift);
		ft_memcpy(pos, item, vec->itemsize);
	}
	vec->nitems++;
	return (1);
}
