/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vreset.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 16:01:48 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:05:27 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"
#include <stdlib.h>

void	ft_vreset(t_vector *vec)
{
	char	*pos;
	size_t	i;

	if (vec->delete)
	{
		i = 0;
		pos = vec->data;
		while (i < vec->nitems)
		{
			vec->delete(pos);
			i++;
			pos += vec->itemsize;
		}
	}
	free(vec->data);
	vec->data = NULL;
	vec->nitems = 0;
	vec->maxitems = 0;
}
