/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vpush.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 14:55:12 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 14:57:47 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

int		ft_vpush(t_vector *vec, const void *item)
{
	char	*pos;

	if (vec->nitems == vec->maxitems)
		if (!ft_vrealloc(vec))
			return (0);
	if (vec->shift == UNSHIFTABLE)
		pos = VECTOR_MUL(vec, vec->nitems);
	else
		pos = VECTOR_SHL(vec, vec->nitems);
	ft_memcpy(pos, item, vec->itemsize);
	vec->nitems++;
	return (1);
}
