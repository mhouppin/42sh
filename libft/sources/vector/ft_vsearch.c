/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vsearch.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 16:08:20 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:25:21 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

void	*ft_vsearch_slow(t_vector *vec, const void *elem)
{
	char	*ptr;
	size_t	left;
	size_t	right;
	size_t	index;
	int		result;

	left = 0;
	right = vec->nitems;
	while (left < right)
	{
		index = (left + right) >> 1;
		ptr = VECTOR_MUL(vec, index);
		result = vec->compar(elem, ptr);
		if (result < 0)
			right = index;
		else if (result > 0)
			left = index + 1;
		else
			return (ptr);
	}
	return (NULL);
}

void	*ft_vsearch_fast(t_vector *vec, const void *elem)
{
	char	*ptr;
	size_t	left;
	size_t	right;
	size_t	index;
	int		result;

	left = 0;
	right = vec->nitems;
	while (left < right)
	{
		index = (left + right) >> 1;
		ptr = VECTOR_SHL(vec, index);
		result = vec->compar(elem, ptr);
		if (result < 0)
			right = index;
		else if (result > 0)
			left = index + 1;
		else
			return (ptr);
	}
	return (NULL);
}

void	*ft_vsearch(t_vector *vec, const void *elem)
{
	if (vec->shift == UNSHIFTABLE)
		return (ft_vsearch_slow(vec, elem));
	else
		return (ft_vsearch_fast(vec, elem));
}
