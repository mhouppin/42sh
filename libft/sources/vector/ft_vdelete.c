/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vdelete.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/21 11:57:32 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/21 12:42:34 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

void	ft_vdelete(t_vector *vec, size_t index)
{
	char	*where;

	if (vec->shift == UNSHIFTABLE)
		where = VECTOR_MUL(vec, index);
	else
		where = VECTOR_SHL(vec, index);
	if (vec->delete != NULL)
		vec->delete(where);
	vec->nitems--;
	ft_memcpy(where, where + vec->itemsize, vec->nitems - index);
}
