/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vrealloc.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 16:26:35 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:29:41 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"
#include "ft_math.h"
#include <stdlib.h>

int		ft_vrealloc(t_vector *vec)
{
	size_t	new_size;
	char	*tmp;

	new_size = vec->maxitems + ft_zsqrt(vec->maxitems) + 1;
	tmp = (char *)malloc(new_size * vec->itemsize);
	if (tmp == NULL)
		return (0);
	ft_memcpy(tmp, vec->data, new_size * vec->itemsize);
	free(vec->data);
	vec->data = tmp;
	vec->maxitems = new_size;
	return (1);
}
