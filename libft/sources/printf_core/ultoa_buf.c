/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ultoa_buf.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 11:55:36 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 11:57:21 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

void	ultoa_buf(char **ptr, uintmax_t value, uintmax_t base)
{
	unsigned char	byte;

	while (1)
	{
		byte = value % base;
		value /= base;
		if (byte < 10)
			**ptr = byte + '0';
		else
			**ptr = byte + 'a' - 0xa;
		(*ptr)--;
		if (value == 0)
			break ;
	}
}
