/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   int_sharp.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 14:27:25 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 14:33:04 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		int_sharp(t_array *to_fill, t_format *format)
{
	if (to_fill->used_size == 0 && format->specifier == 'o')
	{
		to_fill->data[to_fill->used_size++] = '0';
		return (0);
	}
	if ((to_fill->used_size == 0 || (*to_fill->data == '0' &&
		to_fill->used_size == 1)) && format->specifier != 'p')
		return (0);
	if (format->specifier == 'o')
	{
		if (!ft_bpush_front_c(to_fill, '0'))
			return (-1);
	}
	else
	{
		if (!ft_bpush_front(to_fill, "0x", 2))
			return (-1);
	}
	return (0);
}
