/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   va_load_u.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 11:39:32 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 14:41:59 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

uintmax_t	va_load_u(t_format *format, va_list ap)
{
	uintmax_t	val;

	if (format->flags & Length_z)
		return (va_arg(ap, size_t));
	else if (format->flags & Length_j)
		return (va_arg(ap, uintmax_t));
	else if (format->flags & Length_ll)
		return (va_arg(ap, unsigned long long int));
	else if (format->flags & Length_l)
		return (va_arg(ap, unsigned long int));
	else
	{
		val = va_arg(ap, unsigned int);
		if (format->flags & Length_hh)
			return ((unsigned char)val);
		else if (format->flags & Length_h)
			return ((unsigned short)val);
		else
			return (val);
	}
}
