/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_chr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 14:07:59 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 14:10:51 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		print_chr(t_array *to_fill, t_format *format, va_list ap)
{
	(void)format;
	to_fill->data[0] = (char)va_arg(ap, int);
	to_fill->used_size = 1;
	return (0);
}
