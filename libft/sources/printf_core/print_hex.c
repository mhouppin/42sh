/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_hex.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 14:57:30 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 15:12:55 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"
#include "ft_ctype.h"

int		print_hex(t_array *to_fill, t_format *format, va_list ap)
{
	size_t		i;
	uintmax_t	value;

	value = va_load_u(format, ap);
	get_uint(to_fill, value, 16);
	if (format->flags & FieldPrecision)
		if (int_set_precision(to_fill, format))
			return (-1);
	if (format->flags & FlagZero)
		if (int_zero_padding(to_fill, format, 0))
			return (-1);
	if (format->flags & FlagSharp)
		if (int_sharp(to_fill, format))
			return (-1);
	if (format->specifier == 'X')
	{
		i = 0;
		while (i < to_fill->used_size)
		{
			to_fill->data[i] = ft_toupper(to_fill->data[i]);
			i++;
		}
	}
	return (0);
}
