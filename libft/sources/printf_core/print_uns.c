/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_uns.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 11:31:43 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 11:51:30 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		print_uns(t_array *to_fill, t_format *format, va_list ap)
{
	uintmax_t	value;

	value = va_load_u(format, ap);
	get_uint(to_fill, value, 10);
	if (format->flags & FieldPrecision)
		if (int_set_precision(to_fill, format))
			return (-1);
	if (format->flags & FlagZero)
		if (int_zero_padding(to_fill, format, 0))
			return (-1);
	return (0);
}
