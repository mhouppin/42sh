/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_uint.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 11:47:27 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 15:24:59 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

void	get_uint(t_array *to_fill, uintmax_t value, uintmax_t base)
{
	char	*ptr;
	size_t	size;

	ptr = (char *)to_fill->data + 22;
	ultoa_buf(&ptr, value, base);
	size = (size_t)((char *)to_fill->data + 22 - ptr);
	ft_memcpy(to_fill->data, ptr + 1, size);
	to_fill->used_size = size;
}
