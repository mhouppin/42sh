/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   apply_width.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 13:18:42 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/12 09:35:16 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

const char	*apply_width(t_array *buffer, t_array *conv, t_format *info,
			const char *fmt)
{
	const size_t	convsize = conv->used_size;

	if (!ft_bpush_back(buffer, conv->data, convsize))
	{
		ft_bquit(conv);
		return (NULL);
	}
	ft_bquit(conv);
	if ((int)convsize < info->width)
	{
		if (info->flags & FlagMinus)
		{
			if (!ft_bset_back(buffer, (info->flags & FlagZero) ? '0' : ' ',
				(size_t)info->width - convsize))
				return (NULL);
		}
		else if (!ft_bset(buffer, buffer->used_size - convsize,
			(info->flags & FlagZero) ? '0' : ' ',
			(size_t)info->width - convsize))
			return (NULL);
	}
	return (fmt);
}
