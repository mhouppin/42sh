/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_int.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 14:37:43 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 14:56:29 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		print_int(t_array *to_fill, t_format *format, va_list ap)
{
	intmax_t	value;

	value = va_load_i(format, ap);
	get_uint(to_fill, (value < 0) ? -value : value, 10);
	if (format->flags & FieldPrecision)
		if (int_set_precision(to_fill, format))
			return (-1);
	if (format->flags & FlagZero)
		if (int_zero_padding(to_fill, format, value < 0))
			return (-1);
	if (value < 0)
	{
		if (!ft_bpush_front_c(to_fill, '-'))
			return (-1);
	}
	else if (format->flags & FlagPlus)
	{
		if (!ft_bpush_front_c(to_fill, '+'))
			return (-1);
	}
	else if (format->flags & FlagSpace)
		if (!ft_bpush_front_c(to_fill, ' '))
			return (-1);
	return (0);
}
