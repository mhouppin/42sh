/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   int_zero_padding.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 12:27:43 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 12:33:30 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		int_zero_padding(t_array *to_fill, t_format *format, int sign)
{
	int	padsize;

	padsize = format->width - (int)to_fill->used_size;
	format->flags &= ~(FlagZero);
	if (format->flags & FlagSharp)
	{
		if (format->specifier == 'o')
			padsize -= 1;
		else if (format->specifier == 'x' || format->specifier == 'X')
			padsize -= 2;
	}
	padsize -= ((format->flags & (FlagPlus | FlagSpace)) || sign);
	if (padsize <= 0)
		return (0);
	if (!ft_bset_front(to_fill, '0', (size_t)padsize))
		return (-1);
	return (0);
}
