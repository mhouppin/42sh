/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   va_load_i.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 14:39:04 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 14:57:02 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"
#include <unistd.h>

intmax_t	va_load_i(t_format *format, va_list ap)
{
	intmax_t	val;

	if (format->flags & Length_z)
		return (va_arg(ap, ssize_t));
	else if (format->flags & Length_j)
		return (va_arg(ap, intmax_t));
	else if (format->flags & Length_t)
		return (va_arg(ap, ptrdiff_t));
	else if (format->flags & Length_ll)
		return (va_arg(ap, long long int));
	else if (format->flags & Length_l)
		return (va_arg(ap, long int));
	else
	{
		val = va_arg(ap, int);
		if (format->flags & Length_hh)
			return ((signed char)val);
		else if (format->flags & Length_h)
			return ((short)val);
		else
			return (val);
	}
}
