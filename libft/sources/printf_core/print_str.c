/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_str.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 14:10:15 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 14:18:29 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		print_str(t_array *to_fill, t_format *format, va_list ap)
{
	char	*str;
	size_t	size;

	str = va_arg(ap, char *);
	if (str == NULL)
		str = (char *)"(null)";
	if (format->flags & FieldPrecision)
		size = ft_strnlen(str, (size_t)format->precision);
	else
		size = ft_strlen(str);
	if (!ft_bpush_back(to_fill, str, size))
		return (-1);
	return (0);
}
