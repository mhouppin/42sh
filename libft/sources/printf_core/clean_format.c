/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   clean_format.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 15:05:49 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/12 09:28:30 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

void	clean_format(t_format *format)
{
	if (format->flags & FlagMinus)
		format->flags &= ~(FlagZero);
	if (format->flags & FlagPlus)
		format->flags &= ~(FlagSpace);
	if (ft_strchrnul("cdisu", format->specifier))
		format->flags &= ~(FlagSharp);
	if (ft_strchrnul("uoxX", format->specifier))
		format->flags &= ~(FlagSpace | FlagPlus);
	if (ft_strchrnul("DOUF", format->specifier))
	{
		format->specifier += 'a' - 'A';
		format->flags &= ~(Length_all);
		format->flags |= Length_l;
	}
	if (format->specifier == 'p')
	{
		format->flags &= ~(Length_all);
		format->flags |= Length_l | FlagSharp;
	}
	if (format->precision < 0)
		format->precision = 0;
}
