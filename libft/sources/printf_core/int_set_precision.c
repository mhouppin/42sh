/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   int_set_precision.c                              .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 12:02:06 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 15:18:44 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		int_set_precision(t_array *to_fill, t_format *format)
{
	size_t	size;

	format->flags &= ~(FlagZero);
	if (format->precision == 0 && to_fill->data[0] == '0')
	{
		to_fill->used_size = 0;
		return (0);
	}
	size = to_fill->used_size + ((format->flags & FlagSharp)
		&& format->specifier == 'o');
	if (size > (size_t)format->precision)
		return (0);
	if (!ft_bset_front(to_fill, '0', format->precision - size))
		return (-1);
	return (0);
}
