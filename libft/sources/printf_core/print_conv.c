/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_conv.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 11:06:37 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/12 09:50:31 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

static int	(*g_format_table[58])(t_array *, t_format *, va_list) =
{
	NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, &print_hex, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, &print_bin, &print_chr, &print_int, NULL,
	NULL, NULL, NULL, &print_int, NULL,
	NULL, NULL, NULL, NULL, &print_oct,
	&print_hex, NULL, NULL, &print_str, NULL,
	&print_uns, NULL, NULL, &print_hex, NULL, NULL
};

static int		get_flags(const char **fmt)
{
	int		flags;

	flags = 0;
	while (1)
	{
		if (**fmt == '0')
			flags |= FlagZero;
		else if (**fmt == '+')
			flags |= FlagPlus;
		else if (**fmt == '-')
			flags |= FlagMinus;
		else if (**fmt == ' ')
			flags |= FlagSpace;
		else if (**fmt == '#')
			flags |= FlagSharp;
		else
			break ;
		(*fmt)++;
	}
	return (flags);
}

static int		get_width(const char **fmt, va_list ap)
{
	int		width;

	if (**fmt == '*')
	{
		(*fmt)++;
		return (va_arg(ap, int));
	}
	width = 0;
	while (**fmt >= '0' && **fmt <= '9')
	{
		width *= 10;
		width += **fmt;
		width -= '0';
		(*fmt)++;
	}
	return (width);
}

static int		get_precision(const char **fmt, int *flags, va_list ap)
{
	int		precision;

	precision = 0;
	if (**fmt != '.')
		return (precision);
	(*fmt)++;
	*flags |= FieldPrecision;
	if (**fmt == '*')
	{
		(*fmt)++;
		return (va_arg(ap, int));
	}
	while (**fmt >= '0' && **fmt <= '9')
	{
		precision *= 10;
		precision += **fmt;
		precision -= '0';
		(*fmt)++;
	}
	return (precision);
}

static int		get_length(const char **fmt)
{
	int		length;

	length = 0;
	while (1)
	{
		if (**fmt == 'h')
			length |= (length & Length_h) ? Length_hh : Length_h;
		else if (**fmt == 'l')
			length |= (length & Length_l) ? Length_ll : Length_l;
		else if (**fmt == 'j')
			length |= Length_j;
		else if (**fmt == 't')
			length |= Length_t;
		else if (**fmt == 'z')
			length |= Length_z;
		else
			break ;
		(*fmt)++;
	}
	return (length);
}

const char		*print_conv(t_array *buffer, const char *fmt, va_list ap)
{
	t_format	info;
	t_array		*convert;

	fmt++;
	if ((convert = ft_binit(64)) == NULL)
		return (NULL);
	info.flags = get_flags(&fmt);
	info.width = get_width(&fmt, ap);
	info.precision = get_precision(&fmt, &(info.flags), ap);
	info.flags |= get_length(&fmt);
	info.specifier = *fmt;
	clean_format(&info);
	fmt += (*fmt != '\0');
	if (info.specifier < 'A' || info.specifier > 'z' ||
		g_format_table[info.specifier - 'A'] == NULL)
	{
		convert->data[0] = info.specifier;
		convert->used_size = (info.specifier != '\0');
	}
	else if (g_format_table[info.specifier - 'A'](convert, &info, ap))
	{
		ft_bquit(convert);
		return (NULL);
	}
	return (apply_width(buffer, convert, &info, fmt));
}
