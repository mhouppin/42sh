/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_bin.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 14:03:04 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 14:07:14 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "printf_core.h"

int		print_bin(t_array *to_fill, t_format *format, va_list ap)
{
	uintmax_t	value;

	value = va_load_u(format, ap);
	get_uint(to_fill, value, 2);
	if (format->flags & FieldPrecision)
		if (int_set_precision(to_fill, format))
			return (-1);
	if (format->flags & FlagZero)
		if (int_zero_padding(to_fill, format, 0))
			return (-1);
	return (0);
}
