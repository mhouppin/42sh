/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_swap.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 16:17:11 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:18:23 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

void	ft_swap(void *left, void *right, size_t size)
{
	size_t	i;
	char	tmp;

	i = 0;
	while (i < size)
	{
		tmp = ((char *)left)[i];
		((char *)left)[i] = ((char *)right)[i];
		((char *)right)[i] = tmp;
		i++;
	}
}
