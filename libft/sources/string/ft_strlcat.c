/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strlcat.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:15:04 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 16:17:16 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dstlen;
	size_t	srclen;

	dstlen = ft_strnlen(dst, size);
	srclen = ft_strlen(src);
	if (dstlen != size)
	{
		if (srclen >= size - dstlen)
		{
			ft_memcpy(dst + dstlen, src, size - dstlen - 1);
			dst[size - 1] = '\0';
		}
		else
			ft_memcpy(dst + dstlen, src, srclen + 1);
	}
	return (dstlen + srclen);
}
