/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_stpncpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:02:11 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 16:04:10 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_stpncpy(char *dst, const char *src, size_t len)
{
	size_t	i;
	size_t	z;

	i = 0;
	while (src[i] && i < len)
	{
		dst[i] = src[i];
		i++;
	}
	z = i;
	while (z < len)
	{
		dst[z] = '\0';
		z++;
	}
	return (dst + i);
}
