/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strcat.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:10:54 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 16:11:57 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strcat(char *s1, const char *s2)
{
	char	*s1p;

	s1p = s1 + ft_strlen(s1);
	while (*s2)
	{
		*s1p = *s2;
		s1p++;
		s2++;
	}
	*s1p = '\0';
	return (s1);
}
