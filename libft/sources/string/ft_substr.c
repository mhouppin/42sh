/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_substr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/18 15:00:03 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/28 16:52:32 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_substr(const char *str, size_t start, size_t maxlen)
{
	if (ft_strnlen(str, start) < start)
		return (ft_strdup(""));
	return (ft_strndup(str + start, maxlen));
}
