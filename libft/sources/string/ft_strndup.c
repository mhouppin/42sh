/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strndup.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 15:50:57 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 15:54:21 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"
#include <stdlib.h>

char	*ft_strndup(const char *s1, size_t n)
{
	size_t	size;
	char	*scopy;

	size = ft_strnlen(s1, n);
	scopy = (char *)malloc(size + 1);
	if (scopy == NULL)
		return (NULL);
	ft_memcpy(scopy, s1, size);
	scopy[size] = '\0';
	return (scopy);
}
