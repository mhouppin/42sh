/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memrcpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 15:18:19 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 15:22:32 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

void	*ft_memrcpy(void *dst, const void *src, size_t n)
{
	while (n--)
		((char *)dst)[n] = ((const char *)src)[n];
	return (dst);
}
