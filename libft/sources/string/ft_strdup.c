/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strdup.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 15:34:30 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/18 13:12:46 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"
#include <stdlib.h>

char	*ft_strdup(const char *s1)
{
	size_t	size;
	char	*scopy;

	size = ft_strlen(s1) + 1;
	scopy = (char *)malloc(size);
	if (scopy == NULL)
		return (NULL);
	return ((char *)ft_memcpy(scopy, s1, size));
}
