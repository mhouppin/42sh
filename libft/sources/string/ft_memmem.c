/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memmem.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:30:19 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 16:33:48 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

void	*ft_memmem(const void *b, size_t blen, const void *l, size_t llen)
{
	void	*ptr;
	char	lc;

	if (llen == 0)
		return ((void *)b);
	lc = *(const char *)l;
	while (blen >= llen)
	{
		ptr = ft_memchr(b, lc, blen);
		if (ptr == NULL)
			break ;
		blen -= ((size_t)ptr - (size_t)b);
		if (blen < llen)
			break ;
		if (!ft_memcmp(ptr, l, llen))
			return (ptr);
		b = (void *)((char *)ptr + 1);
		blen--;
	}
	return (NULL);
}
