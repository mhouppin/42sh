/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strspn.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 16:38:36 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:40:23 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

size_t	ft_strspn(const char *str, const char *accept)
{
	unsigned char		buf[256];
	const unsigned char	*sp = (const unsigned char *)str;

	ft_memset(buf, 0, 256);
	while (*accept)
		buf[*(unsigned char *)(accept++)] = 1;
	while (buf[*sp] == 1)
		sp++;
	return ((size_t)((const char *)sp - str));
}
