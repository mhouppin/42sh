/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strrchr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:23:11 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 16:25:27 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strrchr(const char *s, int c)
{
	const char	*ptr = NULL;

	while (*s)
	{
		if (*s == c)
			ptr = s;
		s++;
	}
	if (*s == c)
		return ((char *)s);
	return ((char *)ptr);
}
