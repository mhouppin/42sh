/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memwload.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/06 11:39:17 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/06 11:43:08 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

void	*ft_memwload(const void *b, size_t size, size_t access_times)
{
	size_t	i;

	if (access_times == 0)
		return ((void *)b);
	i = 0;
	if (access_times == 1)
		while (i < size)
			__builtin_prefetch((const char *)b + i++, 1, 0);
	else if (access_times <= 4)
		while (i < size)
			__builtin_prefetch((const char *)b + i++, 1, 1);
	else if (access_times <= 16)
		while (i < size)
			__builtin_prefetch((const char *)b + i++, 1, 2);
	else
		while (i < size)
			__builtin_prefetch((const char *)b + i++, 1, 3);
	return ((void *)b);
}
