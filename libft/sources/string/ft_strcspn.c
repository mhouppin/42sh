/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strcspn.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/22 16:36:13 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:40:16 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

size_t	ft_strcspn(const char *str, const char *reject)
{
	unsigned char		buf[256];
	const unsigned char	*sp = (const unsigned char *)str;

	ft_memset(buf, 0, 256);
	while (*reject)
		buf[*(unsigned char *)(reject++)] = 1;
	buf[0] = 1;
	while (buf[*sp] == 0)
		sp++;
	return ((size_t)((const char *)sp - str));
}
