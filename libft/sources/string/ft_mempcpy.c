/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_mempcpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/06/07 14:19:29 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 16:47:46 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

void	*ft_mempcpy(void *dst, const void *src, size_t n)
{
	char		*dstp;
	const char	*srcp;

	dstp = dst;
	srcp = src;
	while (n--)
		*(dstp++) = *(srcp++);
	return (dstp);
}
