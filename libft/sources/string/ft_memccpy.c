/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memccpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 15:13:06 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 15:15:12 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_string.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t	i;
	char	k;

	i = 0;
	while (i < n)
	{
		k = ((const char *)src)[i];
		((char *)dst)[i] = k;
		if (k == (char)c)
			return ((void *)((char *)dst + i + 1));
		i++;
	}
	return (NULL);
}
