/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strnew.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/18 14:53:35 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/18 14:55:28 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdlib.h"
#include "ft_string.h"

char	*ft_strnew(size_t nbytes)
{
	char	*str;

	str = (char *)malloc(nbytes + 1);
	if (str == NULL)
		return (NULL);
	return (ft_memset(str, '\0', nbytes + 1));
}
