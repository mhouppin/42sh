/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_atoi.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 16:40:27 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/07 16:42:40 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdlib.h"

int		ft_atoi(const char *str)
{
	int		ret;
	int		sign;

	while (*str == ' ' || *str == '\t' || *str == '\n' || *str == '\f'
			|| *str == '\r' || *str == '\v')
		str++;
	sign = (*str == '-');
	str += (*str == '-' || *str == '+') ? 1 : 0;
	ret = 0;
	while (*str >= '0' && *str <= '9')
	{
		ret = (ret << 1) + (ret << 3);
		ret += *str - '0';
		str++;
	}
	return (sign ? -ret : ret);
}
