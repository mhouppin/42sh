/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_calloc.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/18 14:42:58 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/18 14:45:47 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdlib.h"
#include "ft_string.h"

void	*ft_calloc(size_t size, size_t nitems)
{
	void	*ptr;

	size *= nitems;
	ptr = malloc(size);
	if (ptr == NULL)
		return (NULL);
	return (ft_memset(ptr, '\0', size));
}
