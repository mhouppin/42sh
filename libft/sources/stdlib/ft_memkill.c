/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memkill.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 09:28:31 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 09:36:23 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

#define KILL_BYTE	0xA5

void	ft_memkill(void **ptr, size_t size)
{
	ft_memset(*ptr, KILL_BYTE, size);
	free(*ptr);
	*ptr = NULL;
}
