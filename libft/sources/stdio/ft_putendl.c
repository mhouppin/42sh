/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putendl.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 09:14:22 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/08 09:16:05 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdio.h"
#include "ft_string.h"

int		ft_putendl(const char *s)
{
	int		ret;

	ret = (int)write(STDOUT_FILENO, s, ft_strlen(s));
	if (ret < 0)
		return (-1);
	if (write(STDOUT_FILENO, "\n", 1) < 0)
		return (-1);
	return (ret + 1);
}
