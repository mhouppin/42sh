/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_dputendl.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 09:46:25 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/08 09:47:16 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdio.h"
#include "ft_string.h"

int		ft_dputendl(int fd, const char *s)
{
	int		ret;

	ret = (int)write(fd, s, ft_strlen(s));
	if (ret < 0)
		return (-1);
	if (write(fd, "\n", 1) < 0)
		return (-1);
	return (ret + 1);
}
