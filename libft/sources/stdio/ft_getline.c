/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_getline.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/10 11:13:33 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/10 11:55:44 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdio.h"
#include "ft_string.h"
#include <stdlib.h>
#include <stdio.h>

static ssize_t	allocator_fail(char **ptr, size_t *ptrsize)
{
	if (*ptr)
		free(*ptr);
	*ptr = NULL;
	*ptrsize = 0;
	return (-1);
}

static ssize_t	truncate_line(char **line, char **ebuffer, size_t *esize,
				char *nl_pointer)
{
	size_t	line_size;

	if (nl_pointer != NULL)
		line_size = (size_t)(nl_pointer - *ebuffer + 1);
	else
		line_size = *esize;
	if (line_size == 0)
	{
		free(*ebuffer);
		*ebuffer = NULL;
		return (0);
	}
	if ((*line = (char *)malloc(line_size + 1)) == NULL)
		return (allocator_fail(ebuffer, esize));
	ft_memcpy(*line, *ebuffer, line_size);
	(*line)[line_size] = '\0';
	*esize -= line_size;
	ft_memcpy(*ebuffer, *ebuffer + line_size, *esize);
	return ((ssize_t)line_size);
}

ssize_t			ft_getline(int fd, char **line)
{
	static char		*ebuffer = NULL;
	static size_t	esize = 0;
	ssize_t			rsize;
	char			*tmp;
	char			buffer[BUFSIZ];

	if (read(fd, NULL, 0) == -1)
		return (-1);
	tmp = ft_memchr(ebuffer, '\n', esize);
	while (tmp == NULL)
	{
		if ((rsize = read(fd, buffer, BUFSIZ)) == 0)
			break ;
		if ((tmp = (char *)malloc(esize + (size_t)rsize)) == NULL)
			return (allocator_fail(&ebuffer, &esize));
		ft_memcpy(tmp, ebuffer, esize);
		ft_memcpy(tmp + esize, buffer, (size_t)rsize);
		if (ebuffer)
			free(ebuffer);
		ebuffer = tmp;
		tmp = ft_memchr(ebuffer + esize, '\n', (size_t)rsize);
		esize += (size_t)rsize;
	}
	return (truncate_line(line, &ebuffer, &esize, tmp));
}
