/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_dputstr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 09:47:19 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/08 09:47:49 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdio.h"
#include "ft_string.h"

int		ft_dputstr(int fd, const char *s)
{
	return ((int)write(fd, s, ft_strlen(s)));
}
