/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putstr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 09:20:03 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/08 09:22:34 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdio.h"
#include "ft_string.h"

int		ft_putstr(const char *s)
{
	return ((int)write(STDOUT_FILENO, s, ft_strlen(s)));
}
