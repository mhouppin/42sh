/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vdprintf.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 10:29:27 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 11:30:43 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdio.h"
#include "ft_string.h"
#include "printf_core.h"

static int	vdprintf_fill(t_array *buffer, const char *fmt, va_list ap)
{
	const char	*tmp;

	while (*fmt)
	{
		if ((tmp = ft_strchr(fmt, '%')) != NULL)
		{
			if (ft_bpush_back(buffer, fmt, (size_t)(tmp - fmt)) == 0)
				return (-1);
			if ((fmt = print_conv(buffer, tmp, ap)) == NULL)
				return (-1);
		}
		else
		{
			if (ft_bpush_back(buffer, fmt, ft_strlen(fmt)) == 0)
				return (-1);
			break ;
		}
	}
	return (0);
}

int			ft_vdprintf(int fd, const char *fmt, va_list ap)
{
	int			retval;
	t_array		*buffer;

	if ((buffer = ft_binit(1024)) == NULL)
		return (-1);
	if (vdprintf_fill(buffer, fmt, ap))
	{
		ft_bquit(buffer);
		return (-1);
	}
	retval = write(fd, buffer->data, buffer->used_size);
	ft_bquit(buffer);
	return (retval);
}
