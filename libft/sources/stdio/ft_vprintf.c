/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_vprintf.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 10:26:32 by mhouppin     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 10:27:04 by mhouppin    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_stdio.h"

int		ft_vprintf(const char *fmt, va_list ap)
{
	return (ft_vdprintf(STDOUT_FILENO, fmt, ap));
}
