# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: mhouppin <mhouppin@student.le-101.>        +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/11/21 11:48:19 by mhouppin     #+#   ##    ##    #+#        #
#    Updated: 2019/11/21 12:01:02 by mhouppin    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

NAME	:= 42sh

ifeq ($(MAKE),)
	MAKE	:= make
endif

SOURCES	:= $(wildcard sources/*/*.c)
OBJECTS	:= $(SOURCES:sources/%.c=objects/%.o)
DEPENDS	:= $(SOURCES:sources/%.c=objects/%.d)

all: $(NAME)

$(NAME): $(OBJECTS) libft/libft.a
	$(CC) -o $@ $^

libft/libft.a:
	$(MAKE) -C libft

objects/%.o: sources/%.c
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	$(CC) -Wall -Wextra -Wpedantic -Wabi -O3 -c -MMD -I include -o $@ $<

-include $(DEPENDS)

clean:
	rm -f $(OBJECTS)
	rm -f $(DEPENDS)
	rm -rf objects

fclean: clean
	make fclean -C libft
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
